#!/bin/sh

### Create a folder for the dune and dumux modules
### Go into the folder and execute this script

if [ -d dune-common ]; then
  echo "error: A directory named dune-common already exists."
  echo "Aborting."
  exit 1
fi

### Clone the necessary modules
git clone https://gitlab.dune-project.org/core/dune-common.git
git clone https://gitlab.dune-project.org/core/dune-geometry.git
git clone https://gitlab.dune-project.org/core/dune-grid.git
git clone https://gitlab.dune-project.org/core/dune-istl.git
git clone https://gitlab.dune-project.org/core/dune-localfunctions.git
git clone https://gitlab.dune-project.org/extensions/dune-alugrid.git
git clone https://git.iws.uni-stuttgart.de/dumux-repositories/dumux.git dumux
git clone https://git.iws.uni-stuttgart.de/dumux-appl/dumux-adaptivestaggered.git
git clone https://git.imp.fu-berlin.de/agnumpde/dune-subgrid.git


### Go to specific branches
cd dune-common && git checkout 05fe487dd34cfc8b0e095477cc4ce682002c18b1 && cd ..
cd dune-geometry && git checkout 2a66558feebde80a5454845495f21a9643b3c76b && cd ..
cd dune-grid && git checkout 369cac45cf0fb27eb9181018509d74c38f07fdf6 && cd ..
cd dune-istl && git checkout 375e9b4b0d14961d496588b98468083c82d50fe8 && cd ..
cd dune-localfunctions && git checkout f93600dfd94fa0124ead0d56101f0482ed049e15 && cd ..
cd dune-alugrid && git checkout 178a69b69eca8bf3e31ddce0fd8c990fee4931ae && cd ..
cd dumux-adaptivestaggered && git checkout 705fe3ce366338b902054716e1bdab6a5b9b05a4 && cd ..
cd buntic2020b && git checkout master && cd ..
cd dune-subgrid && git checkout 45d1ee9f3f711e209695deee97912f4954f7f280 && cd ..

### Go to specific commits
cd dumux && git checkout 12289a1ceaa9d11153b904fc7818d237507f3952 && cd ..

### Run dunecontrol
./dune-common/bin/dunecontrol --opts=dumux/cmake.opts all
