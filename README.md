SUMMARY
=======
This is the DuMuX module containing the code for producing the results for the Forschungsmodul 2 "Combination of a locally-refined quadtree finite-volume staggered grid scheme for the Navier-Stokes equation with turbulence models" of Ivan Buntic.

Installation
============

The easiest way to install this module and its dependencies is to create a new
directory and clone this module:

```
mkdir New_Folder
cd New_Folder
git clone https://git.iws.uni-stuttgart.de/dumux-pub/buntic2020b.git
```

After that, execute the file [installBuntic2020b.sh](https://git.iws.uni-stuttgart.de/dumux-pub/buntic2020b/-/blob/master/installBuntic2020b.sh)

```
cd buntic2020b
git checkout master
cd ..
chmod u+x buntic2020b/installBuntic2020b.sh
./buntic2020b/installBuntic2020b.sh
```

This should automatically download all necessary modules and check out the correct versions.

Applications
============


__Building from source__:

In order run the simulations of the different cases navigate to the folder

```bash
cd buntic2020b/build-cmake/appl/freeflow/rans
```

Compile the programm:

```bash
make test_ff_rans_lauferpipe_oneeq
```

And run by:

* ```./test_ff_rans_lauferpipe_oneeq params.input```
