// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup OneEqModel
 * \brief One-equation turbulence problem base class.
 */
#ifndef DUMUX_REFINED_ONEEQ_PROBLEM_HH
#define DUMUX_REFINED_ONEEQ_PROBLEM_HH

#include <dumux/common/properties.hh>
#include <dumux/common/staggeredfvproblem.hh>
#include <dumux/discretization/localview.hh>
#include <dumux/discretization/staggered/elementsolution.hh>
#include <dumux/discretization/method.hh>
#include <dumux/freeflow/rans/refinedproblem.hh>
#include <dumux/freeflow/turbulencemodel.hh>

#include <dumux/freeflow/rans/oneeq/model.hh>

namespace Dumux {

/*!
 * \ingroup OneEqModel
 * \brief One-equation turbulence problem base class.
 *
 * This implements some base functionality for one-equation Spalart-Allmaras model.
 */
template<class TypeTag>
class RefinedRANSProblemImpl<TypeTag, TurbulenceModel::oneeq> : public RefinedRANSProblemBase<TypeTag>
{

    using ParentType = RefinedRANSProblemBase<TypeTag>;
    using Implementation = GetPropType<TypeTag, Properties::Problem>;
    using GridView = typename GetPropType<TypeTag, Properties::GridGeometry>::GridView;
    using Grid = typename GridView::Grid;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;
    using DimVector = Dune::FieldVector<Scalar, Grid::dimension>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GetPropType<TypeTag, Properties::GridGeometry>::LocalView;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using Element = typename GridView::template Codim<0>::Entity;

    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using PrimaryVariables = GetPropType<TypeTag, Properties::PrimaryVariables>;
    using CellCenterPrimaryVariables = GetPropType<TypeTag, Properties::CellCenterPrimaryVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

public:
    //! The constructor sets the gravity, if desired by the user.
    RefinedRANSProblemImpl(std::shared_ptr<const GridGeometry> gridGeometry, const std::string& paramGroup = "")
    : ParentType(gridGeometry, paramGroup)
    {
        useStoredEddyViscosity_ = getParamFromGroup<bool>(this->paramGroup(),
                                                          "RANS.UseStoredEddyViscosity", false);
    }

    /*!
     * \brief Correct size of the static (solution independent) wall variables
     */
    void updateStaticWallProperties()
    {
        ParentType::updateStaticWallProperties();

        // update size and initial values of the global vectors
        storedDynamicEddyViscosity_.resize(this->gridGeometry().elementMapper().size(), 0.0);
        storedViscosityTilde_.resize(this->gridGeometry().elementMapper().size(), 0.0);
        storedViscosityTildeGradient_.resize(this->gridGeometry().elementMapper().size(), DimVector(0.0));
    }

    /*!
     * \brief Update the dynamic (solution dependent) relations to the walls
     *
     * \param curSol The solution vector.
     */
    template <class SolutionVector, class ElementFaceVariables>
    void updateDynamicWallProperties(const SolutionVector& curSol, const ElementFaceVariables& elemFaceVars)
    {
        ParentType::updateDynamicWallProperties(curSol, elemFaceVars);

        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            unsigned int elementIdx = this->gridGeometry().elementMapper().index(element);

            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
            for (auto&& scv : scvs(fvGeometry))
            {
                const int dofIdx = scv.dofIndex();
                const auto& cellCenterPriVars = curSol[GridGeometry::cellCenterIdx()][dofIdx];
                PrimaryVariables priVars = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVars);
                auto elemSol = elementSolution<typename GridGeometry::LocalView>(std::move(priVars));
                // NOTE: first update the turbulence quantities
                storedViscosityTilde_[elementIdx] = elemSol[0][Indices::viscosityTildeIdx];
                // NOTE: then update the volVars
                VolumeVariables volVars;
                volVars.update(elemSol, asImp_(), element, scv);
                storedDynamicEddyViscosity_[elementIdx] = volVars.calculateEddyViscosity();
            }
        }

        // calculate cell-center-averaged velocity gradients, maximum, and minimum values
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            unsigned int elementIdx = this->gridGeometry().elementMapper().index(element);

            //set xLeft, xRight, yDown, yUp corresponding to the element boundary
            Scalar xLeft = 0.;
            Scalar xRight = 0.;
            Scalar yDown = 0.;
            Scalar yUp = 0.;

            for (unsigned int i = 0; i < element.geometry().corners(); ++i)
            {
                Scalar eps = 1e-8;
                Scalar x = element.geometry().corner(i)[0];
                Scalar y = element.geometry().corner(i)[1];
                Scalar xCenter = element.geometry().center()[0];
                Scalar yCenter = element.geometry().center()[1];

                if (x < (xCenter - eps))
                    xLeft = x;
                else if (x > (xCenter + eps))
                    xRight = x;
                else
                    DUNE_THROW(Dune::InvalidStateException, "invalid state");

                if (y < (yCenter - eps))
                    yDown = y;
                else if (y > (yCenter + eps))
                    yUp = y;
                else
                    DUNE_THROW(Dune::InvalidStateException, "invalid state");
            }

            //calculate derivatives
            const Scalar downXDerivative = calcDerivative_(1, yDown, element); //x-derivative in the element center, calculated by the normal volvars of the scvfs on the bottom of the element
            const Scalar upXDerivative = calcDerivative_(1, yUp, element); //x-derivative in the element center, calculated by the normal volvars of the scvfs on the top of the element
            const Scalar leftYDerivative = calcDerivative_(0, xLeft, element); //y-derivative in the element center, calculated by the normal volvars of the scvfs on the left of the element
            const Scalar rightYDerivative = calcDerivative_(0, xRight, element); //y-derivative in the element center, calculated by the normal volvars of the scvfs on the right of the element

//fill averages into gradients. The gradient lives in the cell center, as do the values, the gradient is built from. The gradients are thus built from (in the unrefined case) two cell-centered values that are not direct neighbors (checker-board-situation), e.g.
//         ---------
//         |       |
//         |   x   |
//         |       |
// -----------------
// |       |       |
// |       |   *   |
// |       |       |
// -----------------
//         |       |
//         |   x   |
//         |       |
//         ---------
// *: gradient position, x: values, gradient is built from
//
// I decided to take the average of left and right, up and down. Of course there might be cases, when one of the two derivatives would be the better choice (e.g. cases in which the one approximation is O(Delta^2), while the other one is just O(Delta), such as
// -------------------------
// |       |       |       |
// |       |       |       |
// |       |       |       |
// -------------------------
// |       |       |       |
// |       |element|       |
// |       |       |       |
// -------------------------
// |   |   |       |       |
// --------|       |       |
// |   |   |       |       |
// -------------------------
// in which situation the right y-gradient is O(Delta^2), while the left is O(Delta)), but I think it would be too much work to sort out those cases.
            storedViscosityTildeGradient_[elementIdx][0] = 0.5 * (downXDerivative + upXDerivative); //x-derivative in the element center
            storedViscosityTildeGradient_[elementIdx][1] = 0.5 * (leftYDerivative + rightYDerivative); //y-derivative in the element center
        }
    }

public:
    std::vector<Scalar> storedDynamicEddyViscosity_;
    std::vector<Scalar> storedViscosityTilde_;
    std::vector<DimVector> storedViscosityTildeGradient_;
    bool useStoredEddyViscosity_;

private:
    template<class GeometricType1, class GeometricType2>
    bool haveCommonCornerWithGeometryArgs_(const GeometricType1& geometry1, const GeometricType2& geometry2)
    {
        bool commonCorners = false;

        using GlobalPosition = typename std::decay_t<decltype(geometry1.corner(0))>;

        std::vector<GlobalPosition> geometry1Corners;
        for (unsigned int i = 0; i < geometry1.corners(); ++i)
        {
            geometry1Corners.push_back(geometry1.corner(i));
        }

        for (unsigned int i = 0; i < geometry2.corners(); ++i)
        {
            const auto geometry2Corner = geometry2.corner(i);

            if (containerFind(geometry1Corners.begin(), geometry1Corners.end(), geometry2Corner) != geometry1Corners.end())
            {
                commonCorners = true;
            }
        }

        return commonCorners;
    }

    Scalar calcDerivative_(unsigned int directionIndex, Scalar elementBoundaryCoordinate, const Element& element)
    {
        auto fvGeometry = localView(this->gridGeometry());
        fvGeometry.bindElement(element);

        Scalar minuend = 0.;
        Scalar subtrahend = 0.;
        Scalar divisor = 0.;

//         ---------
//         |       |
//         |       |
//         |       |
// -----------------
// |   |   |       |
// ---------element|
// |   |   |       |
// -----------------
//         |       |
//         |       |
//         |       |
//         ---------
// In this example configuration, there are two scvfs on the left. In this case, we take the normalVolVarsDofs corresponding to the upper side from the upper scvf and the once corresponding to the lower side from the lower scvf.
// For example, for the upper scvf, we take
//         ---------
//         |       |
//         |   *   |
//         |       |
// -----------------
// |   |  ||       |
// ---------element|
// |   |   |       |
// -----------------
//         |       |
//         |       |
//         |       |
//         ---------
// while we don't take
//         ---------
//         |       |
//         |       |
//         |       |
// -----------------
// |   |  ||       |
// ---------element|
// |   |   |   *   |
// -----------------
//         |       |
//         |       |
//         |       |
//         ---------

        for (auto&& scvf : scvfs(fvGeometry))
        {
            //choose if scvf is left/right/down/up
            if (scalarCmp(scvf.center()[directionIndex], elementBoundaryCoordinate))
            {
                static constexpr auto dimWorld = GridGeometry::GridView::dimensionworld;
                static constexpr int numPairs = 2 * (dimWorld - 1);

                unsigned int dimIdx = 0;

                for (unsigned int localSubFaceIdx = 0; localSubFaceIdx < numPairs; ++localSubFaceIdx)
                {
                    const SubControlVolumeFace& normalFluxCorrectionFace = this->gridGeometry().scvf(scvf.insideScvIdx(), scvf.pairData(localSubFaceIdx).localNormalFluxCorrectionIndex);
                    if (!haveCommonCornerWithGeometryArgs_(element.geometry(), normalFluxCorrectionFace))
                        continue;

                    dimIdx = normalFluxCorrectionFace.directionIndex();

                    const auto dofs = scvf.volVarsData().normalVolVarsDofs[localSubFaceIdx];
                    const auto interpFacts = scvf.volVarsData().normalVolVarsDofsInterpolationFactors[localSubFaceIdx];
                    const auto scvfBuildingData = scvf.volVarsData().normalVolVarsScvfBuildingData[localSubFaceIdx];

                    for (unsigned int j = 0; j < dofs.size(); ++j)
                    {
                        if (! (dofs[j] < 0))
                        {
                            auto multiplied = storedViscosityTilde_[dofs[j]];
                            multiplied *= interpFacts[j];

                            if (sign(normalFluxCorrectionFace.unitOuterNormal()[dimIdx])>0)
                                minuend += multiplied;
                            else
                                subtrahend += multiplied;
                        }
                        else
                        {
                            const auto& boundaryFace = this->gridGeometry().scvf(scvfBuildingData[j].eIdx, scvfBuildingData[j].localScvfIdx);
                            const auto& boundaryElement = this->gridGeometry().element(scvfBuildingData[j].eIdx);

                            if (asImp_().boundaryTypes(boundaryElement, boundaryFace).isDirichlet(Indices::viscosityTildeIdx))
                            {
                                // face Value
                                Scalar dirichletViscosityTilde = asImp_().dirichlet(element, scvf)[Indices::viscosityTildeIdx];

                                auto multiplied = dirichletViscosityTilde;
                                multiplied *= interpFacts[j];

                                if (sign(normalFluxCorrectionFace.unitOuterNormal()[dimIdx])>0)
                                    minuend += multiplied;
                                else
                                    subtrahend += multiplied;
                            }
                            else
                            {
                                if (normalFluxCorrectionFace.boundary())
                                    return 0.;
                                else
                                {
                                    auto multiplied = storedViscosityTilde_[scvfBuildingData[j].eIdx];
                                    multiplied *= interpFacts[j];

                                    if (sign(normalFluxCorrectionFace.unitOuterNormal()[dimIdx])>0)
                                        minuend += multiplied;
                                    else
                                        subtrahend += multiplied;
                                }
                            }
                        }
                    }

                    divisor += scvf.pairData()[localSubFaceIdx].outerNormalDistance;
                }

                divisor += scvf.area();//OK for 2D
            }
        }

        return (minuend - subtrahend) / divisor;
    }

    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    { return *static_cast<Implementation *>(this); }

    //! \copydoc asImp_()
    const Implementation &asImp_() const
    { return *static_cast<const Implementation *>(this); }
};

} // end namespace Dumux

#endif
