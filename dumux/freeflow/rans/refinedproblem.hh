// -*- mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=4 sw=4 sts=4:
/*****************************************************************************
 *   See the file COPYING for full copying permissions.                      *
 *                                                                           *
 *   This program is free software: you can redistribute it and/or modify    *
 *   it under the terms of the GNU General Public License as published by    *
 *   the Free Software Foundation, either version 3 of the License, or       *
 *   (at your option) any later version.                                     *
 *                                                                           *
 *   This program is distributed in the hope that it will be useful,         *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of          *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the            *
 *   GNU General Public License for more details.                            *
 *                                                                           *
 *   You should have received a copy of the GNU General Public License       *
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.   *
 *****************************************************************************/
/*!
 * \file
 * \ingroup RANSModel
 * \copydoc Dumux::RefinedRANSProblem
 */
#ifndef DUMUX_REFINED_RANS_PROBLEM_HH
#define DUMUX_REFINED_RANS_PROBLEM_HH

#include <dune/common/fmatrix.hh>
#include <dumux/common/properties.hh>
#include <dumux/common/staggeredfvproblem.hh>
#include <dumux/discretization/localview.hh>
#include <dumux/discretization/staggered/elementsolution.hh>
#include <dumux/discretization/method.hh>
#include <dumux/freeflow/navierstokes/myproblem.hh>

#include <dumux/freeflow/rans/model.hh>


#include <dumux/discretization/staggered/freeflow/mystaggeredgeometryhelper.hh>
namespace Dumux {

//! forward declare
template<class TypeTag, TurbulenceModel turbulenceModel>
class RefinedRANSProblemImpl;

//! the turbulence-model-specfic RANS problem
template<class TypeTag>
using RefinedRANSProblem = RefinedRANSProblemImpl<TypeTag, GetPropType<TypeTag, Properties::ModelTraits>::turbulenceModel()>;

/*!
 * \ingroup RANSModel
 * \brief Reynolds-Averaged Navier-Stokes problem base class.
 *
 * This implements some base functionality for RANS models.
 * Especially vectors containing all wall-relevant properties, which are accessed
 * by the volumevariables.
 */
template<class TypeTag>
class RefinedRANSProblemBase : public MyNavierStokesProblem<TypeTag>
{
    using ParentType = MyNavierStokesProblem<TypeTag>;
    using Implementation = GetPropType<TypeTag, Properties::Problem>;

    using Scalar = GetPropType<TypeTag, Properties::Scalar>;

    using GridGeometry = GetPropType<TypeTag, Properties::GridGeometry>;
    using FVElementGeometry = typename GridGeometry::LocalView;
    using GridView = typename GridGeometry::GridView;
    using Element = typename GridView::template Codim<0>::Entity;
    using SubControlVolume = typename FVElementGeometry::SubControlVolume;
    using SubControlVolumeFace = typename FVElementGeometry::SubControlVolumeFace;
    using VolumeVariables = GetPropType<TypeTag, Properties::VolumeVariables>;
    using SolutionVector = GetPropType<TypeTag, Properties::SolutionVector>;
    using PrimaryVariables = typename VolumeVariables::PrimaryVariables;
    using CellCenterPrimaryVariables = GetPropType<TypeTag, Properties::CellCenterPrimaryVariables>;
    using FacePrimaryVariables = GetPropType<TypeTag, Properties::FacePrimaryVariables>;
    using Indices = typename GetPropType<TypeTag, Properties::ModelTraits>::Indices;

    using GlobalPosition = typename SubControlVolumeFace::GlobalPosition;

    static constexpr auto dim = GridView::dimension;
    using DimVector = GlobalPosition;
    using DimMatrix = Dune::FieldMatrix<Scalar, dim, dim>;
    
    
//     using IntersectionMapper = MyNonConformingGridIntersectionMapper<GridView>;
//     using GeometryHelper = MyFreeFlowStaggeredGeometryHelper<GridView, IntersectionMapper>;
    
    

public:
    /*!
     * \brief The constructor
     * \param gridGeometry The finite volume grid geometry
     * \param paramGroup The parameter group in which to look for runtime parameters first (default is "")
     */
    RefinedRANSProblemBase(std::shared_ptr<const GridGeometry> gridGeometry, const std::string& paramGroup = "")
    : ParentType(gridGeometry, paramGroup)
    { }

    /*!
     * \brief Update the static (solution independent) relations to the walls
     *
     * This function determines all element with a wall intersection,
     * the wall distances and the relation to the neighboring elements.
     */
    void updateStaticWallProperties()
    {
        using std::abs;
        std::cout << "Update static wall properties. ";
        calledUpdateStaticWallProperties = true;

        // update size and initial values of the global vectors
        wallElementIdx_.resize(this->gridGeometry().elementMapper().size());
        wallDistance_.resize(this->gridGeometry().elementMapper().size(), std::numeric_limits<Scalar>::max());
        neighborIdx_.resize(this->gridGeometry().elementMapper().size());
        cellCenter_.resize(this->gridGeometry().elementMapper().size(), GlobalPosition(0.0));
        velocity_.resize(this->gridGeometry().elementMapper().size(), DimVector(0.0));
        velocityMaximum_.resize(this->gridGeometry().elementMapper().size(), DimVector(0.0));
        velocityGradients_.resize(this->gridGeometry().elementMapper().size(), DimMatrix(0.0));
        stressTensorScalarProduct_.resize(this->gridGeometry().elementMapper().size(), 0.0);
        vorticityTensorScalarProduct_.resize(this->gridGeometry().elementMapper().size(), 0.0);
        flowNormalAxis_.resize(this->gridGeometry().elementMapper().size(), 0);
        wallNormalAxis_.resize(this->gridGeometry().elementMapper().size(), 1);
        kinematicViscosity_.resize(this->gridGeometry().elementMapper().size(), 0.0);
        sandGrainRoughness_.resize(this->gridGeometry().elementMapper().size(), 0.0);

        stressTensorScalarProductTest_.resize(this->gridGeometry().elementMapper().size(), 0.0);
        vorticityTensorScalarProductTest_.resize(this->gridGeometry().elementMapper().size(), 0.0);
        
        // retrieve all wall intersections and corresponding elements
        std::vector<unsigned int> wallElements;
        std::vector<GlobalPosition> wallPositions;
        std::vector<unsigned int> wallNormalAxisTemp;

        const auto gridView = this->gridGeometry().gridView();
        auto fvGeometry = localView(this->gridGeometry());

        
        auto fvGeometryTest = localView(this->gridGeometry());
        

        
        //loop over all elements and scvfs to build wallPositionsToCheckAgainst_ in "problem.hh"
        for (const auto& elementTest : elements(gridView))
        {
            fvGeometryTest.bindElement(elementTest);
            for (const auto& scvfTest : scvfs(fvGeometryTest))
            {
                // only search for walls at a global boundary
                if (!scvfTest.boundary())
                {
                    continue;
                }

                asImp_().determineWallPositions(scvfTest);
            }
        }
        
        
        for (const auto& element : elements(gridView))
        {
            fvGeometry.bindElement(element);
            for (const auto& scvf : scvfs(fvGeometry))
            {;
                // only search for walls at a global boundary
                if (!scvf.boundary())
                {
                    continue;
                }
                
                if (asImp_().isOnWall(scvf))
                {
                    wallElements.push_back(this->gridGeometry().elementMapper().index(element));
                    wallPositions.push_back(scvf.center());
                    wallNormalAxisTemp.push_back(scvf.directionIndex());
                }
            }
        }
        
        if (wallPositions.size() == 0)
            DUNE_THROW(Dune::InvalidStateException,
                       "No wall intersections have been found. Make sure that the isOnWall(globalPos) is working properly.");

            
        
        //TODO: for better efficiency put this in another element/scvf loop
        // count how many scvfs there are
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
          
            int scvfCounter = 0;

            for (auto&& scvf : scvfs(fvGeometry))
            {
                const auto faceDofIdx = scvf.dofIndex();

                //get number of scvfs   
                if(scvfCounter < faceDofIdx){
                    scvfCounter = faceDofIdx;
                }
            }
            numberOfScvfs_ = scvfCounter;
        }    
            
    

        //output purposes
//-------------------------------------------------------------------------------------------------------------   
//         std::cout << "There are " << numberOfScvfs_ << " scvfs!" << std::endl;
//
//         for (unsigned int j=0; j<wallElements.size(); j++)
//         {
//               std::cout << "wallElements[" << j << "]: " << wallElements[j] << std::endl;
//         }
//         
//         for (unsigned int j=0; j<wallPositions.size(); j++)
//         {
//               std::cout << "wallPositions[" << j << "]: " << wallPositions[j] << std::endl;
//         }
        
//         for (unsigned int j=0; j<wallNormalAxisTemp.size(); j++)
//         {
//               std::cout << "wallNormalAxisTemp[" << j << "]: " << wallNormalAxisTemp[j] << std::endl;
//         }
            
        /*std::cout << std::endl;  
        std::cout << std::endl;  
        std::cout << std::endl;  
        std::cout << std::endl;*/  
//-------------------------------------------------------------------------------------------------------------            
            
        // search for shortest distance to wall for each element
        for (const auto& element : elements(gridView))
        {
            unsigned int elementIdx = this->gridGeometry().elementMapper().index(element);
            cellCenter_[elementIdx] = element.geometry().center();
            
            //for storing final values, set to unrealistically high number
            //TODO: set minimalWallDistance to a relative value which is always fulfilled independent of the geometry
            Scalar minimalWallDistance = 1e100;
            unsigned int minWallElementIdx = 0;
            unsigned int minSearchAxis = 0;
            GlobalPosition minWallPosition;
            
            for (unsigned int i = 0; i < wallPositions.size(); ++i)
            {
                static const int problemWallNormalAxis
                    = getParamFromGroup<int>(this->paramGroup(), "RANS.WallNormalAxis", -1);
                int searchAxis = problemWallNormalAxis;

                // search along wall normal axis of the intersection
                if (problemWallNormalAxis < 0 || problemWallNormalAxis >= dim)
                {
                    searchAxis = wallNormalAxisTemp[i];
                }

                //return wallElement which goes with the element indices stored in "wallElements"
                const auto& wallElement = this->gridGeometry().element(wallElements[i]);
                
                //size of wall-scvf will always be the largest one of the sizes in the respective direction/dimension of an element (no refinement possible at boundary)
                Scalar maxWidth = 0.0;
                Scalar maxHeight = 0.0;
                
                auto fvGeometry = localView(this->gridGeometry());
                fvGeometry.bindElement(wallElement); //in order to iterate over scvfs of an wallElement, not the element itself
                
                //check all acvfs of an element and choose the largest value of their lengths in each dimension
                for (auto&& scvf : scvfs(fvGeometry))
                {
                    const auto dirIdx = scvf.directionIndex();
                    const auto area = scvf.area();

                   if(dirIdx == 0){//check in x-direction
                       if(maxHeight < area){
                           maxHeight = area;
                       }
                   }
                   else if(dirIdx == 1){//check in y-direction
                       if(maxWidth < area){
                           maxWidth = area;
                       }
                   }
                }
                                
                //obtain length/width of wall
                Scalar wallHeight = maxHeight;
                Scalar wallWidth = maxWidth;
                
                std::vector<Scalar> wallElementSizes;
                wallElementSizes.clear();
                wallElementSizes.push_back(wallWidth);
                wallElementSizes.push_back(wallHeight);
                
                // get length of wall to check distance to
                Scalar wallLengthToTestAgainst = wallElementSizes[!searchAxis];
                
                GlobalPosition global = element.geometry().center();
                
                //just for initializing purposes
                GlobalPosition temp = global;
                temp -= wallPositions[i];
                
                //set minDistance to a larger value than temp.two_norm() in the beginning
                Scalar minDistance = 10.0*temp.two_norm();
                
                GlobalPosition tempPos;
                
                                
                //compute distance between element.center and different points on wall (5 points equally distant to each other)
                //"intervall < x" where x is "maximum level of refinement/maximum times of refinement" + 1
                for(unsigned int intervall = 0; intervall <= numberOfRefinementLevels_; intervall++)
                {  
                    global = element.geometry().center(); //start with default value
                    tempPos = wallPositions[i];
                    //iterate over points on wallLengthToTestAgainst according to number of refinement levels
                    tempPos[!searchAxis] -= intervall*wallLengthToTestAgainst/(std::pow(2.0, numberOfRefinementLevels_));
                    
                    global -= tempPos;
                    if(global.two_norm() < minDistance)
                    {
                        minDistance = global.two_norm();
                    }
                    
                    global = element.geometry().center(); //start with default value
                    tempPos = wallPositions[i];
                    //iterate over points on wallLengthToTestAgainst according to number of refinement levels
                    tempPos[!searchAxis] += intervall*wallLengthToTestAgainst/(std::pow(2.0, numberOfRefinementLevels_));
                    
                    global -= tempPos;
                    
                    if(global.two_norm() < minDistance)
                    {
                        minDistance = global.two_norm();
                    }
                }

                
                if(minDistance < minimalWallDistance)
                {
                    minimalWallDistance = minDistance;
                    minWallElementIdx = wallElements[i];
                    minSearchAxis = searchAxis;
                    minWallPosition = wallPositions[i];
                }
            }

            wallDistance_[elementIdx] = minimalWallDistance;
            wallElementIdx_[elementIdx] = minWallElementIdx;
            wallNormalAxis_[elementIdx] = minSearchAxis;
            sandGrainRoughness_[elementIdx] = asImp_().sandGrainRoughnessAtPos(minWallPosition);
        }
        
        
        
        //output purposes
//------------------------------------------------------------------------------------------------------------- 
//         for (unsigned int j=0; j<wallDistance_.size(); j++)
//         {
//               std::cout << "wallDistance_[" << j << "]: " << wallDistance_[j] << std::endl;
//         }
//         
//         std::cout << std::endl;
//         
//         for (unsigned int j=0; j<wallDistance_.size(); j++)
//         {
//               std::cout << "wallElementIdx_[" << j << "]: " << wallElementIdx_[j] << std::endl;
//         }
//         
//         std::cout << std::endl;
//------------------------------------------------------------------------------------------------------------- 

        
        std::cout << std::endl;
        std::cout << std::endl;
        std::cout << std::endl;
    }

    
    
    /*!
     * \brief Update the dynamic (solution dependent) relations to the walls
     *
     * The basic function calcuates the cell-centered velocities and
     * the respective gradients.
     * Further, the kinematic viscosity at the wall is stored.
     *
     * \param curSol The solution vector.
     */
//     void updateDynamicWallProperties(const SolutionVector& curSol)
//     {
    template <class SolutionVector, class ElementFaceVariables>
    void updateDynamicWallProperties(const SolutionVector& curSol, const ElementFaceVariables& elemFaceVars)
    {
        Scalar eps = 1e-10;
        
        using std::abs;
        using std::max;
        using std::min;
        std::cout << "Update dynamic wall properties." << std::endl;
        if (!calledUpdateStaticWallProperties)
            DUNE_THROW(Dune::InvalidStateException,
                       "You have to call updateStaticWallProperties() once before you call updateDynamicWallProperties().");

        static const int flowNormalAxis
            = getParamFromGroup<int>(this->paramGroup(), "RANS.FlowNormalAxis", -1);

        // re-initialize min and max values
        velocityMaximum_.assign(this->gridGeometry().elementMapper().size(), DimVector(1e-16));
        velocityMinimum_.assign(this->gridGeometry().elementMapper().size(), DimVector(std::numeric_limits<Scalar>::max()));
        
        //empty the vectors (in case any entries should be present)
        velocityGradientTemp_.clear();
        
        // store velocities on scvfs
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
            unsigned int elementIdx = this->gridGeometry().elementMapper().index(element);
            
            std::vector<Scalar> tempVelocitiesLocalFaces;
            tempVelocitiesLocalFaces.clear();
            
            // calculate velocities
            DimVector velocityTemp(0.0);
            for (auto&& scvf : scvfs(fvGeometry))
            {
                tempVelocitiesLocalFaces.push_back(elemFaceVars[scvf].velocitySelf());
                
                const int dofIdxFace = scvf.dofIndex();
                const auto numericalSolutionFace = curSol[GridGeometry::faceIdx()][dofIdxFace][Indices::velocity(scvf.directionIndex())];
                velocityTemp[scvf.directionIndex()] += numericalSolutionFace;
            }
            
            for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
                velocity_[elementIdx][dimIdx] = velocityTemp[dimIdx] * 0.5; // faces are equidistant to cell center
        }
        
        
        //initialize vector of vectors for the velocity gradients at the faces
        std::vector<std::vector<Scalar>> veloGradTempInterpDist(numberOfScvfs_ + 1);
    
        
        //loop for calculating and storing interpolated values for the velocity gradients
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
            
            for (auto&& scvf : scvfs(fvGeometry))
            {
                const auto faceDofIdx = scvf.dofIndex();
                const auto faceDofPosition = scvf.center();
                const auto dirIdx = scvf.directionIndex();
                 
                //each veloGradTempInterpDist entry will contain exactly six entries, 0th and 3rd are indicators for the direction of the difference quotient, 1st and 4th are the interpolated values, 2nd and 5th store the distances from the scvf to the interpolated values
                if(!scvf.boundary()){ //for inner scvfs
                    Scalar interpolatedValue; //store the interpolated value of velocitySelf() and velocityOpposite()
                    const auto selfToOppoDist = scvf.selfToOppositeDistance(); //needed for difference quotients
                    
                    interpolatedValue = (elemFaceVars[scvf].velocitySelf() + elemFaceVars[scvf].velocityOpposite())/2.0; //interpolated value is always in the middle of velocitySelf() and velocityOpposite()

                    //check on which side of the scvf we take the interpolated value from (left or right, upper or lower), needed for propper difference quotient
                    if(dirIdx == 0){ //compare positions in 1st dimension
                        if(element.geometry().center()[dirIdx] > faceDofPosition[dirIdx]){
                            veloGradTempInterpDist[faceDofIdx].push_back(1.0); //indicates that interpolated value is on right side of scvf
                            veloGradTempInterpDist[faceDofIdx].push_back(interpolatedValue);
                            veloGradTempInterpDist[faceDofIdx].push_back(0.5*selfToOppoDist);//distance for difference quotient will be sum of both selfToOppoDistances, each divided by 2
                        }
                        else{
                            veloGradTempInterpDist[faceDofIdx].push_back(0.0); //indicates that interpolated value is on left side of scvf
                            veloGradTempInterpDist[faceDofIdx].push_back(interpolatedValue);
                            veloGradTempInterpDist[faceDofIdx].push_back(0.5*selfToOppoDist);   
                        }
                        
                    }
                    else if(dirIdx == 1){ //compare positions in 2nd dimension
                        if(element.geometry().center()[dirIdx] > faceDofPosition[dirIdx]){
                            veloGradTempInterpDist[faceDofIdx].push_back(1.0); //indicates that interpolated value is on upper side of scvf
                            veloGradTempInterpDist[faceDofIdx].push_back(interpolatedValue);
                            veloGradTempInterpDist[faceDofIdx].push_back(0.5*selfToOppoDist);
                        }
                        else{
                            veloGradTempInterpDist[faceDofIdx].push_back(0.0); //indicates that interpolated value is on lower side of scvf
                            veloGradTempInterpDist[faceDofIdx].push_back(interpolatedValue);
                            veloGradTempInterpDist[faceDofIdx].push_back(0.5*selfToOppoDist);   
                        }//TODO: for third dimension insert another else case with if dirIdx == 2
                    }                    
                }
                else{ //if boundary
                    Scalar interpolatedValue; //store the interpolated value of velocitySelf() and velocityOpposite()
                    const auto selfToOppoDistHalf = 0.5*scvf.selfToOppositeDistance(); //needed for difference quotients
                    
                    interpolatedValue = (elemFaceVars[scvf].velocitySelf() + elemFaceVars[scvf].velocityOpposite())/2.0;
                                        
                    if(!asImp_().boundaryTypes(element, scvf).isDirichlet(Indices::velocity(scvf.directionIndex()))){
                        //Dirichlet is NOT given
                        //TODO: does it make sense to set gradients to 0 at boundary? Maybe if nothing else is given
                        veloGradTempInterpDist[faceDofIdx].push_back(0.0);
                        veloGradTempInterpDist[faceDofIdx].push_back(0.0);
                        veloGradTempInterpDist[faceDofIdx].push_back(0.0);
                        veloGradTempInterpDist[faceDofIdx].push_back(0.0);
                        veloGradTempInterpDist[faceDofIdx].push_back(0.0);
                        veloGradTempInterpDist[faceDofIdx].push_back(0.0);
                    }
                    else{
                        //Dirichlet is given
                        //TODO: veloGrad at boundary necessary and not velocity itself?
                        Scalar dirichletVelocity = asImp_().dirichlet(element, scvf)[Indices::velocity(scvf.directionIndex())];
                        
                        veloGradTempInterpDist[faceDofIdx].push_back(1.0); //indicates that interpolated value is on right side of scvf
                        veloGradTempInterpDist[faceDofIdx].push_back(interpolatedValue);
                        veloGradTempInterpDist[faceDofIdx].push_back(selfToOppoDistHalf);//distance for difference quotient will be sum of both selfToOppoDistances, each divided by 2
                        veloGradTempInterpDist[faceDofIdx].push_back(0.0); //indicates that interpolated value is on left side of scvf
                        veloGradTempInterpDist[faceDofIdx].push_back(dirichletVelocity);
                        veloGradTempInterpDist[faceDofIdx].push_back(0.0); 
                    }
                }
            }
        }
        
          
//only for output
//------------------------------------------------------------------------------------------------------------------------------------------------------
//         for(int i=0; i<=numberOfScvfs_; i++){
//             std::cout << "scvfIdx: " << std::setw(3) << i;
//             for(int j=0; j<veloGradTempInterpDist[i].size(); j++){
//                 std::cout << "   " << std::setw(11) << veloGradTempInterpDist[i][j];
//             }
//             std::cout << std::endl;
//         }
//         std::cout << std::endl;
//------------------------------------------------------------------------------------------------------------------------------------------------------
        
        std::vector<Scalar> veloGradAtFaces(numberOfScvfs_ + 1); //+1 because 0 is also an entry
        
        
        //loop for calculating the difference quotients
        for(int i=0; i<=numberOfScvfs_; i++){
            //check if boundary or not, for boundary without given Dirichlet every entry is zero, but distance must not be zero
            if(veloGradTempInterpDist[i][2] + veloGradTempInterpDist[i][5] > eps){
                //this holds for "right-sided asymmetric" difference quotient, check which interpolated value is on which side of the scvf 
                if(1.0-eps < veloGradTempInterpDist[i][0] && veloGradTempInterpDist[i][0] < 1.0+eps){//checks if entry is == 1
                    veloGradAtFaces[i] = (veloGradTempInterpDist[i][1] - veloGradTempInterpDist[i][4])/(veloGradTempInterpDist[i][2] + veloGradTempInterpDist[i][5]);
                }
                else if(0.0-eps < veloGradTempInterpDist[i][0] && veloGradTempInterpDist[i][0] < 0.0+eps){//checks if entry == 0
                    veloGradAtFaces[i] = (veloGradTempInterpDist[i][4] - veloGradTempInterpDist[i][1])/(veloGradTempInterpDist[i][2] + veloGradTempInterpDist[i][5]);
                }
            }
            else{ //this holds for boundary scvfs with no given boundary conditions 
                  //TODO: is this still required? so far yes, if no Dirichelt conditions are given for certain scvf
               veloGradAtFaces[i] = 0.0;   
            }
        }

        
        
        //create suporting structure to help transform gradients into necessary format
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);

            //vector to help transfer velocityGradients dependency from [scvfIdx] to [elementIdx][scvfIdx]
            std::vector<Scalar> transformStructure;
            transformStructure.clear();
            
            for (auto&& scvf : scvfs(fvGeometry))
            {
                const auto faceDofIdx = scvf.dofIndex();

                transformStructure.push_back(veloGradAtFaces[faceDofIdx]);
            }
            
            velocityGradientTemp_.push_back(transformStructure);
        }
        
        
        
        
//only for output
//------------------------------------------------------------------------------------------------------------------------------------------------------
//         std::cout << "==============================================================================================" << std::endl;
//         std::cout << "VeloGrads" << std::endl;
//         for (const auto& element : elements(this->gridGeometry().gridView()))
//         {
//             auto fvGeometry = localView(this->gridGeometry());
//             fvGeometry.bindElement(element);
//             unsigned int elementIdx = this->gridGeometry().elementMapper().index(element);
// 
//             std::cout << "elementIdx: " << std::setw(4) << elementIdx << "::::";
//  
//             for (auto&& scvf : scvfs(fvGeometry))
//             {
//                 const auto localFaceDofIdx = scvf.localFaceIdx();
//  
//                 std::cout << std::setw(11) << velocityGradientTemp_[elementIdx][localFaceDofIdx] << "    ";
//             }
//             
//             std::cout << std::endl;
// 
//         }
//         std::cout << ":::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::" << std::endl;
//------------------------------------------------------------------------------------------------------------------------------------------------------

       
        
        // calculate cell-center-averaged maximum, and minimum values
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            unsigned int elementIdx = this->gridGeometry().elementMapper().index(element);
            unsigned int wallElementIdx = wallElementIdx_[elementIdx];

            Scalar maxVelocity = 0.0;
            for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
            {
                if (abs(velocity_[elementIdx][dimIdx]) > abs(velocityMaximum_[wallElementIdx][dimIdx]))
                {
                    velocityMaximum_[wallElementIdx][dimIdx] = velocity_[elementIdx][dimIdx];
                }
                if (abs(velocity_[elementIdx][dimIdx]) < abs(velocityMinimum_[wallElementIdx][dimIdx]))
                {
                    velocityMinimum_[wallElementIdx][dimIdx] = velocity_[elementIdx][dimIdx];
                }

                if (0 <= flowNormalAxis && flowNormalAxis < dim)
                {
                    flowNormalAxis_[elementIdx] = flowNormalAxis;
                }
                else if (abs(maxVelocity) < abs(velocity_[elementIdx][dimIdx]))
                {
                    maxVelocity = abs(velocity_[elementIdx][dimIdx]);
                    flowNormalAxis_[elementIdx] = dimIdx;
                }
            }
        }
        
        
        // calculate or call all secondary variables
        for (const auto& element : elements(this->gridGeometry().gridView()))
        {
            unsigned int elementIdx = this->gridGeometry().elementMapper().index(element);

            Dune::FieldMatrix<Scalar, GridView::dimension, GridView::dimension> stressTensorTest(0.0);
            Dune::FieldMatrix<Scalar, GridView::dimension, GridView::dimension> vorticityTensorTest(0.0);
            
            auto fvGeometry = localView(this->gridGeometry());
            fvGeometry.bindElement(element);
 
            Scalar tempValue = 0.0;
            int checkDir = 0;
            Scalar checkPos = 0.0;
            Scalar checkPosDiffDim = 0.0;
            std::vector<Scalar> stressTensorKeeper;
            stressTensorKeeper.clear();
            
            for (auto&& scvf : scvfs(fvGeometry))
            {
                const auto faceDofPosition = scvf.center();
                const auto dirIdx = scvf.directionIndex();
                const auto localFaceDofIdx = scvf.localFaceIdx();
 
                
                if(localFaceDofIdx == 0){
                    stressTensorKeeper.push_back(velocityGradientTemp_[elementIdx][localFaceDofIdx]);
                    tempValue = velocityGradientTemp_[elementIdx][localFaceDofIdx];
                }
                else{
                    if(dirIdx == checkDir){
                        if(faceDofPosition[dirIdx] == checkPos /*&& faceDofPosition[!dirIdx] != checkPosDiffDim*/){    
                            tempValue = (tempValue + velocityGradientTemp_[elementIdx][localFaceDofIdx])/2.0; 
                            stressTensorKeeper.pop_back();
                            stressTensorKeeper.push_back(tempValue);
                        }
                        else{
                            stressTensorKeeper.push_back(velocityGradientTemp_[elementIdx][localFaceDofIdx]);
                            tempValue = velocityGradientTemp_[elementIdx][localFaceDofIdx];
                        }
                    }
                    else{
                        stressTensorKeeper.push_back(velocityGradientTemp_[elementIdx][localFaceDofIdx]);
                        tempValue = velocityGradientTemp_[elementIdx][localFaceDofIdx];
                    }    
                }
                checkDir = dirIdx; 
                checkPos = faceDofPosition[dirIdx];
                checkPosDiffDim = faceDofPosition[!dirIdx];
            }
            
            
            stressTensorTest[0][0] = 0.5*(stressTensorKeeper[0]+stressTensorKeeper[0]);
            stressTensorTest[1][0] = 0.5*(stressTensorKeeper[1]+stressTensorKeeper[2]);
            stressTensorTest[0][1] = 0.5*(stressTensorKeeper[2]+stressTensorKeeper[1]);
            stressTensorTest[1][1] = 0.5*(stressTensorKeeper[3]+stressTensorKeeper[3]);
            
            vorticityTensorTest[0][0] = 0.5*(stressTensorKeeper[0]-stressTensorKeeper[0]);
            vorticityTensorTest[1][0] = 0.5*(stressTensorKeeper[1]-stressTensorKeeper[2]);
            vorticityTensorTest[0][1] = 0.5*(stressTensorKeeper[2]-stressTensorKeeper[1]);
            vorticityTensorTest[1][1] = 0.5*(stressTensorKeeper[3]-stressTensorKeeper[3]);
            
            
            stressTensorScalarProductTest_[elementIdx] = 0.0;
            for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
            {
                for (unsigned int velIdx = 0; velIdx < dim; ++velIdx)
                {
                    stressTensorScalarProductTest_[elementIdx] += stressTensorTest[dimIdx][velIdx] * stressTensorTest[dimIdx][velIdx];
                }
            } 
            
            
            vorticityTensorScalarProductTest_[elementIdx] = 0.0;
            for (unsigned int dimIdx = 0; dimIdx < dim; ++dimIdx)
            {
                for (unsigned int velIdx = 0; velIdx < dim; ++velIdx)
                {
                    vorticityTensorScalarProductTest_[elementIdx] += vorticityTensorTest[dimIdx][velIdx] * vorticityTensorTest[dimIdx][velIdx];
                }
            }
            
        
            for (auto&& scv : scvs(fvGeometry))
            {
                const int dofIdx = scv.dofIndex();

                // construct a privars object from the cell center solution vector
                const auto& cellCenterPriVars = curSol[GridGeometry::cellCenterIdx()][dofIdx];
                PrimaryVariables priVars = makePriVarsFromCellCenterPriVars<PrimaryVariables>(cellCenterPriVars);
                auto elemSol = elementSolution<typename GridGeometry::LocalView>(std::move(priVars));

                VolumeVariables volVars;
                volVars.update(elemSol, asImp_(), element, scv);
                kinematicViscosity_[elementIdx] = volVars.viscosity() / volVars.density();
            }
        }
        
        stressTensorScalarProduct_ = stressTensorScalarProductTest_;
        vorticityTensorScalarProduct_ = vorticityTensorScalarProductTest_;
    }

    
    
    
    
    /*!
     * \brief Returns whether a wall function should be used at a given face
     *
     * \param element The element.
     * \param scvf The sub control volume face.
     * \param eqIdx The equation index.
     */
    bool useWallFunction(const Element& element,
                         const SubControlVolumeFace& scvf,
                         const int& eqIdx) const
    { return false; }

    /*!
     * \brief Returns an additional wall function momentum flux
     */
    template<class ElementVolumeVariables, class ElementFaceVariables>
    FacePrimaryVariables wallFunction(const Element& element,
                                      const FVElementGeometry& fvGeometry,
                                      const ElementVolumeVariables& elemVolVars,
                                      const ElementFaceVariables& elemFaceVars,
                                      const SubControlVolumeFace& scvf,
                                      const SubControlVolumeFace& lateralBoundaryFace) const
    { return FacePrimaryVariables(0.0); }

    /*!
     * \brief  Returns an additional wall function flux for cell-centered quantities
     */
    template<class ElementVolumeVariables, class ElementFaceVariables>
    CellCenterPrimaryVariables wallFunction(const Element& element,
                                            const FVElementGeometry& fvGeometry,
                                            const ElementVolumeVariables& elemVolVars,
                                            const ElementFaceVariables& elemFaceVars,
                                            const SubControlVolumeFace& scvf) const
    { return CellCenterPrimaryVariables(0.0); }

    /*!
     * \brief Returns whether a given sub control volume face is on a wall
     *
     * \param scvf The sub control volume face.
     */
    bool isOnWall(const SubControlVolumeFace& scvf) const
    {
        return asImp_().isOnWallAtPos(scvf.center());
    }

    /*!
     * \brief Returns whether a given point is on a wall
     *
     * \param globalPos The position in global coordinates.
     */
    bool isOnWallAtPos(const GlobalPosition &globalPos) const
    {
        // Throw an exception if no walls are implemented
        DUNE_THROW(Dune::InvalidStateException,
                   "The problem does not provide an isOnWall() method.");
    }

    /*!
     * \brief Returns the sand-grain roughness \f$\mathrm{[m]}\f$ at a given position
     *
     * \param globalPos The position in global coordinates.
     */
    Scalar sandGrainRoughnessAtPos(const GlobalPosition &globalPos) const
    {
        return 0.0;
    }

    /*!
     * \brief Returns the Karman constant
     */
    const Scalar karmanConstant() const
    { return 0.41; }

    //! \brief Returns the \f$ \beta_{\omega} \f$ constant
    const Scalar betaOmega() const
    {
        return 0.0708;
    }

    /*!
     * \brief Return the turbulent Prandtl number \f$ [-] \f$ which is used to convert
     *        the eddy viscosity to an eddy thermal conductivity
     */
    Scalar turbulentPrandtlNumber() const
    {
        static const Scalar turbulentPrandtlNumber
            = getParamFromGroup<Scalar>(this->paramGroup(), "RANS.TurbulentPrandtlNumber", 1.0);
        return turbulentPrandtlNumber;
    }

    /*!
     * \brief Return the turbulent Schmidt number \f$ [-] \f$ which is used to convert
     *        the eddy viscosity to an eddy diffusivity
     */
    Scalar turbulentSchmidtNumber() const
    {
        static const Scalar turbulentSchmidtNumber
            = getParamFromGroup<Scalar>(this->paramGroup(), "RANS.TurbulentSchmidtNumber", 1.0);
        return turbulentSchmidtNumber;
    }
    
    
    
    /*!
     * \brief set the number of refinement levels accordingly to main.cc
     */
    void setNumberOfRefinementLevels(int refinementLevels)
    {
        numberOfRefinementLevels_ = refinementLevels;
    }
    
    
    
  
public:
    bool calledUpdateStaticWallProperties = false;
    std::vector<unsigned int> wallElementIdx_;
    std::vector<Scalar> wallDistance_;
    std::vector<std::array<std::array<unsigned int, 2>, dim>> neighborIdx_;
    std::vector<GlobalPosition> cellCenter_;
    std::vector<DimVector> velocity_;
    std::vector<DimVector> velocityMaximum_;
    std::vector<DimVector> velocityMinimum_;
    std::vector<DimMatrix> velocityGradients_;
    std::vector<Scalar> stressTensorScalarProduct_;
    std::vector<Scalar> vorticityTensorScalarProduct_;
    std::vector<unsigned int> wallNormalAxis_;
    std::vector<unsigned int> flowNormalAxis_;
    std::vector<Scalar> kinematicViscosity_;
    std::vector<Scalar> sandGrainRoughness_;
    std::vector<Scalar> velocityTestTest_;
    //for velocityGradients adaptive ////////////////////////////////////////////////////////
    std::vector<std::vector<Scalar>> velocityGradientTemp_;
    std::vector<std::vector<Scalar>> velocityGradientInterpTemp_;
    int numberOfScvfs_;
    std::vector<Scalar> stressTensorScalarProductTest_;
    std::vector<Scalar> vorticityTensorScalarProductTest_;
    
//     IntersectionMapper intersectionMapper_;
    
    Scalar testVariable_;
    int testTestTest_;
    //_______________________________________________________________________________________
    
    
    int numberOfRefinementLevels_;

private:
    //! Returns the implementation of the problem (i.e. static polymorphism)
    Implementation &asImp_()
    { return *static_cast<Implementation *>(this); }

    //! \copydoc asImp_()
    const Implementation &asImp_() const
    { return *static_cast<const Implementation *>(this); }
};

} // end namespace Dumux

#endif
